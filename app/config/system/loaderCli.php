<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 12/08/17
 * Time: 11:37 PM
 */

$loader->registerDirs(
    [
        $config->application->tasksDir
    ],
    true
)->register();