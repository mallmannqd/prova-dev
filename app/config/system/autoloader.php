<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 08/08/17
 * Time: 10:15 PM
 */

spl_autoload_register(function($class){
    if(!preg_match("/^(Exceptions|Webservice|Validators|System)/", $class)){
        return;
    }

    $newclass = str_replace("\\", "/", $class);
    require_once (APP_PATH . '/' . $newclass . '.php');
});