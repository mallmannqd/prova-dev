<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 14/09/17
 * Time: 11:16
 */

namespace System\Utils;

use Exceptions\UserException;

class Functions
{

    static function validaFormatoData($data)
    {
        if (!preg_match('/^[0-3][0-9]-[0-1][0-9]-[2][0][0-9][0-9]$/', $data)) throw new UserException('Formato de data inválido!');
    }

    static function validaHorarioData(\DateTime $data)
    {
        $dataAgora = new \DateTime();
        if ($data->getTimestamp() < $dataAgora->getTimestamp()) throw new UserException('Data já passada');
    }

    static function validaFormatoHora($hora)
    {
        if (!preg_match('/[0-2][0-9]:[0-5][0-9]/', $hora)) throw new UserException('Formato de hora inválido!');
    }
}