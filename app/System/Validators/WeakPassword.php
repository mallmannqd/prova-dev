<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 20/08/17
 * Time: 10:42 PM
 */

namespace System\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class WeakPassword extends Validator implements ValidatorInterface
{
    private $comum = [
        //Senhas comuns 2016
        '123456',  '123456789', 'qwerty', '12345678', '111111', '1234567890', '1234567', 'password', '123123',
        '987654321', 'qwertyuiop', 'mynoob', '123321', '666666', '18atcskd2w', '7777777', '1q2w3e4r', '654321',
        '555555',  '3rjs1la7qe', 'google', '1q2w3e4r5t', '123qwe', 'zxcvbnm', '1q2w3e',

        //Senhas comuns 2015
        '123456', 'password', '12345678', 'qwerty', '12345', '123456789', 'football', '1234', '1234567', 'baseball',
        'welcome', '1234567890', 'abc123', '111111', '1qaz2wsx', 'dragon', 'master', 'monkey', 'letmein', 'login',
        'princess', 'qwertyuiop', 'solo', 'passw0rd', 'starwars',

        //Senhas comuns 2014
        '123456', 'password', '12345', '12345678', 'qwerty', '1234567890', '1234', 'baseball', 'dragon', 'football',
        '1234567', 'monkey', 'letmein', 'abc123', '111111', 'mustang', 'access', 'shadow', 'master', 'michael',
        'superman', '696969', '123123', 'batman', 'trustno1',

        //Custom
        '102030', '201125',
    ];

    public function validate(Validation $validator, $attribute)
    {
        $password = $validator->getValue($attribute);
        $label = $validator->getLabel($attribute);

        if(!preg_match('/.{6,30}/', $password)){
            $message = $this->getOption('message')[$label];
            if (!$message) {
                $message = "A Senha deve conter entre 6 e 30 caracteres";
            }

            $validator->appendMessage(new Message($message, $attribute));
            return false;
        }


        if(in_array($password, $this->comum)) {
            $message = $this->getOption('message')[$label];
            if (!$message) {
                $message = "Senha muito comum";
            }

            $validator->appendMessage(new Message($message, $attribute));
            return false;
        }

        return true;
    }
}