<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 20/08/17
 * Time: 10:09 PM
 */

namespace System\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class PositiveInt extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $validator->getLabel($attribute);

        if(is_numeric($value) AND $value > 0){
            return true;
        }

        $message = $this->getOption('message')[$label];
        if (!$message) {
            $message = "Campo {$label} deve ser maior do que zero";
        }

        $validator->appendMessage(new Message($message, $attribute));

        return false;
    }
}