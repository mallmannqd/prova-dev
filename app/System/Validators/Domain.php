<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 20/08/17
 * Time: 11:18 PM
 */

namespace System\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class Domain extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $validator->getLabel($attribute);

        if(filter_var('admin@' . $value, FILTER_VALIDATE_EMAIL)){
            return true;
        }

        $message = $this->getOption('message')[$label];
        if (!$message) {
            $message = "Campo {$label} deve conter um domínio válido";
        }

        $validator->appendMessage(new Message($message, $attribute));

        return false;
    }
}