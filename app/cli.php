<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    //Autoloader da aplicação e do composer, respectivamente
    include APP_PATH . '/config/system/autoloader.php';
    include APP_PATH . '/../vendor/autoload.php';

    // Using the CLI factory default services container
    $di = new CliDI();

    /**
     * Read services
     */
    include APP_PATH . '/config/services/base.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/system/loader.php';
    include APP_PATH . '/config/system/loaderCli.php';

    /*
     * System Functions
     */
    include APP_PATH . '/config/system/functions.php';


    // Create a console application
    $console = new ConsoleApp();
    $console->setDI($di);

    /**
     * Process the console arguments
     */
    $arguments = [];

    foreach ($argv as $k => $arg) {
        if ($k === 1) {
            $arguments["task"] = $arg;
        } elseif ($k === 2) {
            $arguments["action"] = $arg;
        } elseif ($k >= 3) {
            $arguments["params"][] = $arg;
        }
    }

    // Handle incoming arguments
    $console->handle($arguments);

} catch (\Phalcon\Exception $e) {
    echo $e->getMessage() . PHP_EOL;
    exit(255);
}