<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 14:34
 */

namespace Exceptions;


class SysException extends \Exception
{
    /**
     * @param string $message
     * @param int $code
     * @param \Exception|NULL $previous
     */
    public function __construct($message, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}