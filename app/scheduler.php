<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 13/08/17
 * Time: 12:32 AM
 */

/**
 *  "* * * * * /usr/bin/php /app/app/scheduler.php 1>> /dev/null 2>&1"
 */

//Autoloadr do Composer
require_once __DIR__.'/../vendor/autoload.php';

use GO\Scheduler;
$scheduler = new Scheduler();

$scheduler->run();