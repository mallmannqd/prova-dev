{{ partial('partials/navbar') }}
{{ partial('partials/breadcumbs') }}

{{ this.getContent() }}

<a href="/index/novo/"><button type="button" class="btn btn-success">Novo Agendamento</button></a>

<div class="col-md-2 pull-right">
    {{ select("sala", salas, 'using': ['id', 'nome'], 'class': 'form-control') }}
</div>
<hr/>

<div id="calendar">

</div>