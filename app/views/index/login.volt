<div class="col-md-12">
    <h2>Login</h2>
    <hr>
</div>
<div class="col-md-6">
    {{ this.getContent() }}

    {{ this.tag.form(["/session/login/", "role" : "form"]) }}
        <div class="form-group">
            <label for="usuario">Usuário</label>
            {{this.tag.emailField(["email", "class" : "form-control", "placeholder" : "jane.doe@example.com"]) }}
        </div>
        <div class="form-group">
            <label for="senha">Senha</label>
            {{this.tag.passwordField(["senha", "class" : "form-control", "placeholder" : "*******"]) }}
        </div>
            {{ this.tag.linkTo(["/index/cadastro", "Cadastre-se"]) }}
        <div class="form-group">
            {{ this.tag.submitButton(["Logar", "class" : "btn btn-default"]) }}
        </div>
    {{ this.tag.endForm() }}
</div>