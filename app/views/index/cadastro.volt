<div class="col-md-12">
    <h2>Cadastre-se</h2>
    <hr>
</div>
<div class="col-md-6">
    {{ this.getContent() }}

    {{ this.tag.form(["/usuarios/cadastra/", "role" : "form"]) }}
    <div class="form-group">
        <label for="usuario">Nome</label>
        {{this.tag.textField(["nome", "class" : "form-control", "placeholder" : "João da Silva"]) }}
    </div>
    <div class="form-group">
        <label for="usuario">E-mail</label>
        {{this.tag.emailField(["email", "class" : "form-control", "placeholder" : "jane.doe@example.com"]) }}
    </div>
    <div class="form-group">
        <label for="senha">Senha</label>
        {{this.tag.passwordField(["senha", "class" : "form-control", "placeholder" : "*******"]) }}
    </div>
    <div class="form-group">
        {{ this.tag.submitButton(["Cadastrar", "class" : "btn btn-default"]) }}
    </div>
    {{ this.tag.endForm() }}
</div>