{{ partial('partials/navbar') }}
{{ partial('partials/breadcumbs') }}

<h2>{{ titulo }}</h2>
<div id="flash-content">{{ this.getContent()|nl2br }}</div>
<hr>

{{ this.tag.form(["agendamentos/agenda"]) }}
<p>
    {{ this.tag.hiddenField("id") }}
</p>

<div class="col-md-4 form-group">
    <label for="email">Sala</label>
    {{ select("sala", salas, 'using': ['id', 'nome'], 'class': 'form-control') }}
</div>

<div class="row"></div>

{#Visualmente é melhor usar o datepicker do jquery UI do que o dateField do Phalcon#}
<div class="col-md-2 form-group">
    <label for="nome">Data</label>
    {{ this.tag.textField(['data', 'class' : 'form-control', 'placeholder' : data ]) }}
</div>

<div class="col-md-2 form-group">
    <label for="nome">Hora</label>
    {{ this.tag.textField(['hora', 'class' : 'form-control', 'placeholder' : '11:35']) }}
</div>

<div class="row"></div>

<div class="col-md-4 form-group">
    {{ this.tag.submitButton(['Agendar', 'class' : 'btn btn-success']) }}
</div>
{{ this.tag.endForm() }}