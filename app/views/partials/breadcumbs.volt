<ol class="breadcrumb">
    {% for link in links %}
       <li class="breadcrumb-item active">
           {% if link['active'] %}
               {{ this.tag.linkTo(link['location'],link['name']) }}
           {% else %}
                {{ link['name'] }}
           {% endif %}
       </li>
    {% endfor %}
</ol>