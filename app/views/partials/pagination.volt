{% set disableLast = (paginator.current == paginator.last) ? 'disabled' : '' %}
{% set disableFirst = (paginator.current == paginator.first) ? 'disabled' : '' %}

<div class="row">
    <nav class="col-sm-12">
        <ul class="pagination">
            <li class="{{ disableFirst }}">{{ this.tag.linkTo(dispatcher.getControllerName() ~ '/' ~ dispatcher.getActionName() ~ '/1', '<span aria-hidden="true">Primeira</span>') }}</li>
            <li class="{{ disableFirst }}">{{ this.tag.linkTo(dispatcher.getControllerName() ~ '/' ~ dispatcher.getActionName() ~ '/' ~ paginator.before, '<span aria-hidden="true">&laquo;</span>') }}</li>

            {% for page in pages %}
                {% if page > 0 %}
                    {% set class = (paginator.current == page) ? 'active' : '' %}
                    <li class="{{ class }}">{{ this.tag.linkTo(dispatcher.getControllerName() ~ '/' ~ dispatcher.getActionName() ~ '/' ~ page, page) }}</li>
                {% endif %}
            {% endfor %}

            <li class="{{ disableLast }}">{{ this.tag.linkTo(dispatcher.getControllerName() ~ '/' ~ dispatcher.getActionName() ~ '/' ~ paginator.next, '<span aria-hidden="true">&raquo;</span>') }}</li>
            <li class="{{ disableLast }}">{{ this.tag.linkTo(dispatcher.getControllerName() ~ '/' ~ dispatcher.getActionName() ~ '/' ~ paginator.last, '<span aria-hidden="true">Último</span>') }}</li>
        </ul>
    </nav>
</div>