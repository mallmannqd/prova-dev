<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/">Dev_Prova</a>
        </div>
        <div id="navbar2" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="{% if navbarlink == 'usuarios' %} active {% endif %}"><a href="/admin/">Usuários</a></li>
                <li class="{% if navbarlink == 'salas' %} active {% endif %}"><a href="/salas/">Salas</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/session/logoutAdmin/"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>