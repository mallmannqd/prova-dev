{{ partial('partials/navbar_admin') }}
{{ partial('partials/breadcumbs') }}

<hr/>
<div>{{ this.getContent()|nl2br }}</div>

<div class="panel panel-default">
    <div class="panel-heading">Novo Usuário</div>
    <div class="panel-body">
        {{ this.tag.form(["usuarios/novoUsuario", "role" : "form", "class" : ""]) }}

        <div class="form-group">
            <label class="control-label col-md-1" for="">Email</label>
            <div class="col-md-3">
                {{this.tag.emailField(["email", "class" : "form-control", "placeholder" : "jane.doe@example.com"]) }}
            </div>

            <label class="control-label col-md-1" for="">Nome</label>
            <div class="col-md-3">
                {{this.tag.textField(["nome", "class" : "form-control", "placeholder" : "João da Silva"]) }}
            </div>

            <label class="control-label col-md-1" for="">Senha</label>
            <div class="col-md-3">
                {{this.tag.passwordField(["senha", "class" : "form-control", "placeholder" : "*******"]) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-1">
                {{ this.tag.submitButton(["Adicionar", "class" : "btn btn-success"]) }}
            </div>
        </div>

        {{ this.tag.endForm() }}
    </div>
</div>

{% if pages|length > 0 %}
    {{ partial('partials/pagination') }}
{% endif %}

<table class="table">
    <thead>
    <tr>
        <td>id</td>
        <td>Email</td>
        <td>Nome</td>
    </tr>
    </thead>
    <tbody>
    {% for usuario in paginator.items %}
        <tr>
            <td>{{ usuario.id }}</td>
            <td>{{ usuario.email }}</td>
            <td>{{ usuario.nome }}</td>
            <td>
                <div class="dropdown">
                    <a href="#" id="dLabel" class="table-button" title="Ações" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Ações<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li>{{ this.tag.linkTo('/admin/edita/' ~ usuario.id, '<span class="glyphicon glyphicon-pencil"></span> Editar Cliente') }}</li>
                        <li><a href="javascript:void(0);" onclick="remover({{ usuario.id }});"> <span class="glyphicon glyphicon-remove"></span> Remover Cliente</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<script>
    function remover(id){
        if(!confirm('Tem certeza que deseja remover este usuário?')){
            return;
        }
        window.location.href = "{{ url('/usuarios/deleta/') }}" + id;
    }
</script>