{{ partial('partials/navbar_admin') }}
{{ partial('partials/breadcumbs') }}

<h2>{{ titulo }}</h2>
<div id="flash-content">{{ this.getContent()|nl2br }}</div>
<hr>

{{ this.tag.form(["usuarios/atualiza"]) }}
<p>
    {{ this.tag.hiddenField("id") }}
</p>

<div class="col-md-4 form-group">
    <label for="email">Email</label>
    {{ this.tag.emailField(['email', 'class' : 'form-control']) }}
</div>

<div class="row"></div>

<div class="col-md-4 form-group">
    <label for="nome">Nome</label>
    {{ this.tag.textField(['nome', 'class' : 'form-control']) }}
</div>

<div class="row"></div>

<div class="col-md-4 form-group">
    {{ this.tag.submitButton(['Atualizar', 'class' : 'btn btn-success']) }}
</div>
{{ this.tag.endForm() }}