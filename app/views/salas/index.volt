{{ partial('partials/navbar_admin') }}
{{ partial('partials/breadcumbs') }}

<hr/>
<div>{{ this.getContent()|nl2br }}</div>

<div class="panel panel-default">
    <div class="panel-heading">Nova Sala</div>
    <div class="panel-body">
        {{ this.tag.form(["salas/novaSala", "role" : "form", "class" : ""]) }}

        <div class="form-group">
            <label class="control-label col-md-1" for="">Nome</label>
            <div class="col-md-3">
                {{this.tag.textField(["nome", "class" : "form-control", "placeholder" : "304B"]) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-1">
                {{ this.tag.submitButton(["Adicionar", "class" : "btn btn-success"]) }}
            </div>
        </div>

        {{ this.tag.endForm() }}
    </div>
</div>

{% if pages|length > 0 %}
    {{ partial('partials/pagination') }}
{% endif %}

<table class="table">
    <thead>
    <tr>
        <td>id</td>
        <td>Nome</td>
    </tr>
    </thead>
    <tbody>
    {% for sala in paginator.items %}
        <tr>
            <td>{{ sala.id }}</td>
            <td>{{ sala.nome }}</td>
            <td>
                <div class="dropdown">
                    <a href="#" id="dLabel" class="table-button" title="Ações" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Ações<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li>{{ this.tag.linkTo('salas/edita/' ~ sala.id, '<span class="glyphicon glyphicon-pencil"></span> Editar Sala') }}</li>
                        <li><a href="javascript:void(0);" onclick="remover({{ sala.id }});"> <span class="glyphicon glyphicon-remove"></span> Remover Sala</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<script>
    function remover(id){
        if(!confirm('Tem certeza que deseja remover este usuário?')){
            return;
        }
        window.location.href = "{{ url('salas/deleta/') }}" + id;
    }
</script>