<?php

use Exceptions\SysException;
use Exceptions\UserException;

/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 12:04
 */

class SessionController extends ControllerBase
{
    function loginAdminAction()
    {
        try{
            $this->validaPost();
            $usuario = $this->request->getPost('usuario');
            $senha = $this->request->getPost('senha');

            $admin = Admin::findFirstByEmail($usuario);
            $this->validaSenha($senha,$admin->getSenha());

            $this->session->set(
                'auth',
                [
                    'email' => $admin->getEmail()
                ]
            );

            $this->flash->success('Bem vindo '.$admin->getEmail());
            return $this->response->redirect('/admin/');

        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/login/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/login');
        }
    }

    function loginAction()
    {
        try{
            $this->validaPost();
            $email = $this->request->getPost('email');
            $senha = $this->request->getPost('senha');

            $usuario = Usuario::findFirstByEmail($email);

            if (!$usuario->getAtivo()) throw new UserException('Confirme seu cadastro no e-mail!');

            $this->validaSenha($senha,$usuario->getSenha());
            $this->session->set(
                'user',
                [
                    'id'    => $usuario->getId(),
                    'email' => $usuario->getEmail(),
                    'nome'  => $usuario->getNome()
                ]
            );

            $this->flash->success('Bem vindo '.$usuario->getNome());
            return $this->response->redirect('/index/');

        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/login/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/login/');
        }
    }

    function logoutAdminAction()
    {
        $this->session->remove('auth');
        $this->flash->success('Deslogado com sucesso');
        return $this->response->redirect('/admin/');
    }

    function logoutAction()
    {
        $this->session->remove('user');
        $this->flash->success('Deslogado com sucesso');
        return $this->response->redirect('/index/');
    }
}