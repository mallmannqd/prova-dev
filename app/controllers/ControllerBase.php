<?php

use Exceptions\SysException;
use Exceptions\UserException;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected $links;

    public function initialize() {
        $this->flash->output();
    }

    function validaSenha($password, $modelPassword)
    {
        if(!$this->security->checkHash($password, $modelPassword)) throw new UserException('Usuário ou senha incorreto');
    }

    function validaPost()
    {
        if (!$this->request->isPost()) throw new SysException('Requisição inválida!');
    }

    function validaAdmin()
    {
        if (!$this->session->has('auth')) throw new SysException('Login necessário!');
    }

    function adminEstaLogado()
    {
        return $this->session->has('auth');
    }

    function validaUsuario()
    {
        if (!$this->session->has('user')) throw new SysException('Login necessário!');
    }

    function usuarioEstaLogado()
    {
        return $this->session->has('user');
    }
}