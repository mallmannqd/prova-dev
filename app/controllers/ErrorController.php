<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 11:51
 */

class ErrorController extends ControllerBase
{

    function show404Action()
    {
        $this->response->setStatusCode(404, 'Not Found');
        $this->view->pick('errors/404');
    }

}