<?php

use Exceptions\SysException;
use Exceptions\UserException;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 12/09/2017
 * Time: 18:36
 */

class SalasController extends ControllerBase
{

    function indexAction($currentPage = 1)
    {
        try{
            $this->validaAdmin();

            $this->links = [
                [
                    'name' => 'Salas'
                ]
            ];

            $salas = Sala::find();

            $paginator = new PaginatorModel(
                [
                    'data' => $salas,
                    'limit' => 10,
                    'page' => $currentPage
                ]
            );

            $page = $paginator->getPaginate();

            if($page->total_pages > 1){
                $firstPage = (($page->current + 4) > $page->total_pages) ? ($page->total_pages - 4) : $page->current ;
                $lastPage = (($page->current + 5) > $page->total_pages) ? $page->total_pages : ($page->current + 4);
                $pages = range($firstPage, $lastPage);
            }

            $this->view->paginator = $page;
            $this->view->pages = $pages;

            $this->view->links = $this->links;
            $this->view->navbarlink = 'salas';
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }
    }

    function novaSalaAction()
    {
        try{
            $this->validaAdmin();
            $sala = new Sala();
            $sala->setNome($this->request->getPost('nome'));

            $sala->validateFields();
            $sala->save();
            $sala->verificaErros();

            $this->flash->success('Sala adicionada com sucesso!');
            return $this->response->redirect('/salas/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/salas/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }
    }

    function editaAction($id)
    {
        try{
            $this->validaAdmin();
            $sala = Sala::findFirstById($id);

            $this->tag->displayTo('id', $sala->getId());
            $this->tag->displayTo('nome', $sala->getNome());

            $this->links = [
                [
                    'name' => 'Salas',
                    'location' => '/salas/',
                    'active' => true
                ],
                [
                    'name' => 'Edita'
                ]
            ];

            $this->view->links = $this->links;
            $this->view->navbarlink = 'salas';
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/salas/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/admin/');
        }
    }

    function atualizaAction()
    {
        try{
            $this->validaAdmin();

            $sala = Sala::findFirstById($this->request->getPost('id'));
            $sala->setNome($this->request->getPost('nome'));

            $sala->validateFields();
            $sala->save();
            $sala->verificaErros();

            $this->flash->success('Sala atualizada!');
            $this->response->redirect('/salas/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/salas/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/admin/');
        }
    }

    function deletaAction($id)
    {
        try{
            $sala = Sala::findFirstById($id);
            $sala->delete();

            $this->flash->success('Sala deletada com sucesso!');
            $this->response->redirect('/salas/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/salas/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/admin/');
        }
    }
}