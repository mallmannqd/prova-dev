<?php

use Exceptions\SysException;
use Exceptions\UserException;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 11:55
 */

class AdminController extends ControllerBase
{

    function indexAction($currentPage = 1)
    {

        if (!$this->adminEstaLogado()){
            return $this->dispatcher->forward(
                [
                    'controller' => 'admin',
                    'action'     => 'login'
                ]
            );
        }

        $usuarios = Usuario::find();

        $paginator = new PaginatorModel(
            [
                'data' => $usuarios,
                'limit' => 10,
                'page' => $currentPage
            ]
        );

        $page = $paginator->getPaginate();

        if($page->total_pages > 1){
            $firstPage = (($page->current + 4) > $page->total_pages) ? ($page->total_pages - 4) : $page->current ;
            $lastPage = (($page->current + 5) > $page->total_pages) ? $page->total_pages : ($page->current + 4);
            $pages = range($firstPage, $lastPage);
        }

        $this->links = [
            [
                'name' => 'Usuarios'
            ]
        ];

        $this->view->paginator = $page;
        $this->view->pages = $pages;
        $this->view->links = $this->links;
        $this->view->navbarlink = 'usuarios';
    }

    function editaAction($id)
    {

        try{

            $this->validaAdmin();
            $usuario = Usuario::findFirstById($id);

            $this->tag->displayTo('id', $usuario->getId());
            $this->tag->displayTo('email', $usuario->getEmail());
            $this->tag->displayTo('nome', $usuario->getNome());

            $this->links = [
                [
                    'name' => 'Usuarios',
                    'location' => '/admin/',
                    'active' => true
                ],
                [
                    'name' => 'Edita'
                ]
            ];

            $this->view->titulo = 'Edita Usuário';
            $this->view->links = $this->links;
            $this->view->navbarlink = 'usuarios';

        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }

    }

    function loginAction()
    {
        if ($this->adminEstaLogado()){
            return $this->response->redirect('/admin/');
        }
    }

}