<?php

use Exceptions\SysException;
use Exceptions\UserException;

/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 30/08/2017
 * Time: 21:23
 */

class IndexController extends ControllerBase
{
    function indexAction()
    {
        $this->assets->addCss("/js/fullcalendar/fullcalendar.css");
        $this->assets->addJs('/js/fullcalendar/lib/moment.min.js');
        $this->assets->addJs('/js/fullcalendar/full2calendar.js');
        $this->assets->addJs('/js/control.js');

        if (!$this->usuarioEstaLogado()){
            return $this->dispatcher->forward(
                [
                    'controller' => 'index',
                    'action'     => 'login'
                ]
            );
        }

        $this->links = [
            [
                'name' => 'Agendamentos'
            ]
        ];

        $this->view->salas = Sala::find();
        $this->view->links = $this->links;
        $this->view->navbarlink = 'agendamentos';
    }

    function loginAction()
    {
        if ($this->usuarioEstaLogado()){
            return $this->response->redirect('/index/');
        }
    }

    function novoAction()
    {
        try {

            $this->assets->addJs("/js/mask/jquery.mask.js");
            $this->assets->addJs("/js/na.js");
            $this->links = [
                [
                    'name' => 'Agendamentos',
                    'location' => '/index/',
                    'active' => true
                ],
                [
                    'name' => 'Novo'
                ]
            ];

            $this->view->salas = Sala::find();
            $this->view->titulo = 'Novo Agendamento';
            $this->view->links = $this->links;
            $this->view->data = date('d-m-Y');

        } catch (SysException $e) {
            $this->flash->error($e->getMessage());
            $this->response->redirect('/index/');
        }
    }

    function cadastroAction()
    {
        
    }

}