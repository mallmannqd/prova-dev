<?php

use Exceptions\SysException;
use Exceptions\UserException;
use System\Utils\Functions;

/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 13/09/17
 * Time: 13:31
 */

class AgendamentosController extends ControllerBase
{

    function buscaAgendamentosAction()
    {
        try{
            $this->view->disable();

            $usuario = $this->session->get('user')['id'];

            if (is_numeric($this->request->getPost('sala'))){
                $sala = $this->request->getPost('sala');
            }

            $agendamentos = Agendamento::find(
                [
                    'id_sala = :id_sala:',
                    'bind' => [
                        'id_sala' => $sala
                    ]
                ]
            );

            $eventos = $this->formataEventos($agendamentos, $usuario);

            echo json_encode($eventos);
        }catch (UserException $e){
            echo json_encode(['success' => false, 'message' => 'Erro: '.$e->getMessage()]);
        }catch (SysException $e){
            echo json_encode(['success' => false, 'message' => 'Erro: '.$e->getMessage()]);
        }

    }

    function agendaAction()
    {
        try{
            $this->validaUsuario();
            $idUsuario = $this->session->get('user')['id'];

            $data = $this->request->getPost('data');
            Functions::validaFormatoData($data);
            $hora = $this->request->getPost('hora');
            Functions::validaFormatoHora($hora);
            $idSala = $this->request->getPost('sala');

            $dataInicio = DateTime::createFromFormat('d-m-Y H:i', $data . $hora);
            Functions::validaHorarioData($dataInicio);

            $dataFim = DateTime::createFromFormat('d-m-Y H:i', $data . $hora);
            $dataFim->add(new DateInterval('PT59M'));

            $dataInicio = $dataInicio->format('Y-m-d H:i');
            $dataFim = $dataFim->format('Y-m-d H:i');

            $agendamento = new Agendamento();

            $agendamento->setDataInicio($dataInicio);
            $agendamento->setDataFim($dataFim);
            $agendamento->setIdUsuario($idUsuario);
            $agendamento->setIdSala($idSala);

            $agendamento->validateFields();
            $agendamento->save();
            $agendamento->verificaErros();

            $this->flash->success('Agendamento efetuado com sucesso!');
            return $this->response->redirect('/index/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/novo/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/');
        }
    }

    function alteraAction()
    {
        try{
            $this->validaUsuario();
            $this->validaPost();
            $this->view->disable();
            $agendamento = Agendamento::findFirst(
                [
                    'id = :id: and id_usuario = :id_usuario:',
                    'bind' => [
                        'id' => $this->request->getPost('id'),
                        'id_usuario' => $this->session->get('user')['id']
                    ]
                ]
            );

            if (!$agendamento) throw new UserException('Agendamento inválido');

            $dataInicio = $this->request->getPost('start');


            $agendamento->setDataInicio($dataInicio);

            Functions::validaHorarioData(DateTime::createFromFormat('Y-m-d H:i', $dataInicio));
            $agendamento->setDataFim($this->request->getPost('end'));

            $agendamento->validateFields();
            $agendamento->save();
            $agendamento->verificaErros();

            echo json_encode(['success' => true, 'message' => 'Agendamento Alterado com sucesso!']);
        }catch (UserException $e){
            echo json_encode(['success' => false, 'message' => 'Erro: '.$e->getMessage()]);
        }catch (SysException $e){
            echo json_encode(['success' => false, 'message' => 'Erro: '.$e->getMessage()]);
        }
    }

    function deletaAction($id)
    {
        try{
            $this->validaUsuario();
            $agendamento = Agendamento::findFirst(
                [
                    'id = :id: and id_usuario = :id_usuario:',
                    'bind' => [
                        'id' => $id,
                        'id_usuario' => $this->session->get('user')['id']
                    ]
                ]
            );


            if (!$agendamento) throw new UserException('Agendamento inválido');

            $agendamento->delete();

            $this->flash->success('Agendamento de sala cancelado com sucesso!');
            return $this->response->redirect('/index/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/index/');
        }
    }

    private function formataEventos($agendamentos, $usuario)
    {
        $eventos = array();

        foreach ($agendamentos as $agendamento) {
            $evento = array();
            $evento['id'] = $agendamento->getId();
            $evento['title'] = 'Sala: '.$agendamento->getSala()->getNome();
            $evento['start'] = $agendamento->getDataInicio();
            $evento['end'] = $agendamento->getDataFim();
            if ($agendamento->getUsuario()->getId() == $usuario){
                $evento['startEditable'] = true;
                $evento['url'] = '/agendamentos/deleta/' . $agendamento->getId();
                $evento['color'] = '#006600';
            }
            array_push($eventos, $evento);
        }
        return $eventos;
    }
}