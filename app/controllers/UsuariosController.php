<?php

use Exceptions\UserException;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 13/09/2017
 * Time: 23:37
 */

class UsuariosController extends ControllerBase
{
    function cadastraAction()
    {
        try{

            $email = $this->request->getPost('email');
            $senha = $this->request->getPost('senha');
            $nome = $this->request->getPost('nome');

            $usuario = new Usuario();
            $usuario->setEmail($email);
            $usuario->setSenha($senha);
            $usuario->setNome($nome);

            $usuario->validateInsert();

            $usuario->setSenha($this->security->hash($senha));
            $usuario->save();
            $usuario->verificaErros();

            $this->enviaEmailCadastro($usuario->getId(), $usuario->getEmail());

            $this->flash->success('Solicitação Enviada! Confirme seu cadastro no seu e-mail.');
            $this->response->redirect('/index/');

        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            $this->response->redirect('/index/');
        }
    }

    function confirmaCadastroAction($hash)
    {
        try{

            $usuario = Usuario::findFirst(
                [
                    "md5(CONCAT('dev', id)) = :hash: and ativo = 0",
                    'bind' => [
                        'hash' => $hash
                    ]
                ]
            );

            $usuario->setAtivo(1);
            $usuario->update();
            $this->flash->success('Cadastro confirmado! Entre com seu login e senha.');
            $this->response->redirect('/index/');
        }catch (UserException $e){
            $this->flash->error('Token inválido, solicite seu cadastro novamente');
            $this->response->redirect('/index/');
        }
    }

    function novoUsuarioAction()
    {
        try{

            $this->validaAdmin();
            $this->validaPost();

            $email = $this->request->getPost('email');
            $senha = $this->request->getPost('senha');
            $nome = $this->request->getPost('nome');

            $usuario = new Usuario();
            $usuario->setEmail($email);
            $usuario->setSenha($senha);
            $usuario->setNome($nome);
            $usuario->setAtivo(1);

            $usuario->validateInsert();
            $usuario->setSenha($this->security->hash($senha));
            $usuario->save();
            $usuario->verificaErros();

            $this->flash->success('Usuário adicionado com sucesso!');
            return $this->response->redirect('/admin/index');

        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/index');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/index');
        }

    }

    function atualizaAction()
    {
        try{

            $email = $this->request->getPost('email');
            $nome = $this->request->getPost('nome');

            $this->validaAdmin();
            $this->validaPost();

            $usuario = Usuario::findFirstById($this->request->getPost('id'));
            $usuario->setEmail($email);
            $usuario->setNome($nome);

            $usuario->validateUpdate();
            $usuario->save();
            $usuario->verificaErros();

            $this->flash->success('Usuário atualizado com sucesso!');
            return $this->response->redirect('/admin/index');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }
    }

    function deletaAction($id)
    {
        try{
            $this->validaAdmin();
            $usuario = Usuario::findFirstById($id);
            $usuario->delete();

            $this->flash->success('Usuário deletado com sucesso!');
            $this->response->redirect('/admin/');
        }catch (UserException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }catch (SysException $e){
            $this->flash->error($e->getMessage());
            return $this->response->redirect('/admin/');
        }
    }

    /**
     * @param $id
     * @param $email
     * @throws UserException
     */
    private function enviaEmailCadastro($id, $email)
    {
        $mail = new PHPMailer();

        $link = 'http://' . $_SERVER['HTTP_HOST'] . '/usuarios/confirmacadastro/' . md5('dev' . $id);
        $mail->isSMTP();
        $mail->Host = 'smtp.cyberweb.com.br';
        $mail->SMTPAuth = true;
        $mail->Port = 587;
        $mail->SMTPSecure = false;
        $mail->SMTPAutoTLS = false;
        $mail->Username = 'guilherme.costa@cyberweb.com.br';
        $mail->Password = 'asm15052014';

        $mail->Sender = 'guilherme.costa@cyberweb.com.br';
        $mail->From = 'guilherme.costa@cyberweb.com.br';
        $mail->FromName = 'Prova-Dev solicitação de Cadastro';

        $mail->addAddress($email);

        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';

        $mail->Subject = "Cadastro no sistema Prova-Dev";
        $mail->Body = "Clique no link para ativar o seu cadastro: <br>";
        $mail->Body .= '<a href='.$link.'>Confirmar Cadastro</a>';

        if (!$mail->send()) throw new UserException('Erro ao enviar e-mail, favor solicitar ao administrador, guilhermecmallmann@gmail.com, novo cadastro no site');
    }

}