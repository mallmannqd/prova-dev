<?php

use Exceptions\UserException;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;
use System\Validators\WeakPassword;

/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 15:17
 */

class Usuario extends AbstractModel
{

    private $id;
    private $nome;
    private $email;
    private $senha;
    private $ativo;

    public function initialize()
    {
        $this->hasMany(
            'id',
            'Agendamento',
            'id_usuario',
            [
                'foreignKey' => [
                    'action' => Relation::ACTION_CASCADE
                ]
            ]
        );
        $this->setSource('usuarios');
    }

    public static function findFirst($parameters = null)
    {
        if (!parent::findFirst($parameters)){
            throw new UserException('Usuário inválido');
        }
        return parent::findFirst($parameters);
    }

    public function validateInsert()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'message' => 'Email inválido!'
                ]
          )
        );

        $validator->add(
            ['senha'],
            new WeakPassword()
        );

        $validator->add(
            'nome',
            new PresenceOf(
                [
                    'message' => 'Nome obrigatório!'
                ]
            )
        );

        $validator->add(
            'email',
            new Uniqueness(
                [
                    'message' => 'Email em uso!'
                ]
            )
        );

        $this->validate($validator);
        $this->verificaErros();

        return true;
    }

    public function validateUpdate()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'message' => 'Email inválido!'
                ]
            )
        );

        $validator->add(
            'nome',
            new PresenceOf(
                [
                    'message' => 'Nome obrigatório!'
                ]
            )
        );

        $validator->add(
            'email',
            new Uniqueness(
                [
                    'message' => 'Email em uso!'
                ]
            )
        );

        $this->validate($validator);
        $this->verificaErros();

        return true;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param mixed $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }



}