<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 13/09/17
 * Time: 13:55
 */

class Evento
{
    private $id;
    private $title;
    private $start;
    private $end;
    private $color;

    /**
     * Evento constructor.
     * @param $id
     * @param $title
     * @param $start
     * @param $end
     * @param $color
     */
    public function __construct($id, $title, $start, $end, $color)
    {
        $this->id = $id;
        $this->title = $title;
        $this->start = $start;
        $this->end = $end;
        $this->color = $color;
    }


}