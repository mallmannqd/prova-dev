<?php

use Exceptions\UserException;
use Phalcon\Mvc\Model\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;

/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 12/09/2017
 * Time: 18:48
 */

class Sala extends AbstractModel
{

    private $id;
    private $nome;
    private $cor;

    function initialize()
    {
        $this->setSource('salas');
    }

    function validateFields()
    {
        $validator = new Validation();

        $validator->add(
            'nome',
            new PresenceOf(
                [
                    'message' => 'Campo nome é obrigatório!'
                ]
            )
        );

        $validator->add(
            'nome',
            new Uniqueness(
                [
                    'message' => 'Sala já existente'
                ]
            )
        );

        if (strlen($this->nome) > 5){
            $this->appendMessage(new Message('O nome da sala deve ter no máximo 5 caracteres'));
        }

        $this->validate($validator);
        $this->verificaErros();
    }

    public static function findFirst($parameters = null)
    {
        if (!parent::findFirst($parameters)){
            throw new UserException('Sala inválida');
        }
        return parent::findFirst($parameters);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @param mixed $cor
     */
    public function setCor($cor)
    {
        $this->cor = $cor;
    }



}