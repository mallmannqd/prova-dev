<?php

use Exceptions\UserException;

/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/09/17
 * Time: 12:17
 */

class Admin extends AbstractModel
{
    private $id;
    private $email;
    private $senha;

    public static function findFirst($parameters = null)
    {
        if (!parent::findFirst($parameters)){
            throw new UserException('Usuário ou senha incorreto');
        }
        return parent::findFirst($parameters);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }


}