<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Exception as ModelException;
use Phalcon\Security;
use Exceptions\UserException;
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 7/27/2017
 * Time: 11:49 PM
 */
class AbstractModel extends Model
{

    /**
     * @return bool
     * @throws UserException
     */
    public function verificaErros(){
        $errMsg = null;

        if ($this->getMessages()){
            foreach ($this->getMessages() as $message){
                $errMsg .= '» ' . $message. "\n";
            }
        }

        if(!is_null($errMsg)){
            throw new UserException($errMsg);
        }

        return true;
    }
}