<?php

use Exceptions\UserException;

/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 12/09/2017
 * Time: 22:35
 */

class Agendamento extends AbstractModel
{

    private $id;
    private $id_usuario;
    private $id_sala;
    private $data_inicio;
    private $data_fim;

    function initialize()
    {
        $this->hasOne(
            'id_sala',
            'Sala',
            'id'
        );
        $this->hasOne(
            'id_usuario',
            'Usuario',
            'id'
        );
        $this->setSource('agendamentos');
    }

    function validateFields()
    {
        $this->verificaDispUsuario();
        $this->verificaDispSala();
        return true;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return mixed
     */
    public function getIdSala()
    {
        return $this->id_sala;
    }

    /**
     * @param mixed $id_sala
     */
    public function setIdSala($id_sala)
    {
        $this->id_sala = $id_sala;
    }

    /**
     * @return mixed
     */
    public function getDataInicio()
    {
        return $this->data_inicio;
    }

    /**
     * @param mixed $data_inicio
     */
    public function setDataInicio($data_inicio)
    {
        $this->data_inicio = $data_inicio;
    }

    /**
     * @return mixed
     */
    public function getDataFim()
    {
        return $this->data_fim;
    }

    /**
     * @param mixed $data_fim
     */
    public function setDataFim($data_fim)
    {
        $this->data_fim = $data_fim;
    }

    private function verificaDispUsuario()
    {
        if (is_numeric($this->id)){
            $agendamento = self::findFirst(
                [
                    "conditions" => "id_usuario = :id_usuario: AND (:dataInicio: BETWEEN data_inicio AND data_fim OR :dataFim: BETWEEN data_inicio AND data_fim) AND id != :id:",
                    'bind' => [
                        "id"         => $this->id,
                        "id_usuario" => $this->id_usuario,
                        "dataInicio" => $this->data_inicio,
                        "dataFim"    => $this->data_fim
                    ]
                ]
            );
        }else{
            $agendamento = self::findFirst(
                [
                    "conditions" => "id_usuario = :id_usuario: AND (:dataInicio: BETWEEN data_inicio AND data_fim OR :dataFim: BETWEEN data_inicio AND data_fim)",
                    'bind' => [
                        "id_usuario" => $this->id_usuario,
                        "dataInicio" => $this->data_inicio,
                        "dataFim"    => $this->data_fim
                    ]
                ]
            );
        }

        if ($agendamento) throw new UserException('Você já tem uma sala agendada neste período');
    }

    private function verificaDispSala()
    {
        if (is_numeric($this->id)) {
            $agendamento = self::findFirst(
                [
                    "conditions" => "id_sala = :id_sala: AND (:dataInicio: BETWEEN data_inicio AND data_fim OR :dataFim: BETWEEN data_inicio AND data_fim ) AND id != :id:",
                    'bind' => [
                        "id"         => $this->id,
                        "id_sala"    => $this->id_sala,
                        "dataInicio" => $this->data_inicio,
                        "dataFim"    => $this->data_fim
                    ]
                ]
            );
        }else{
            $agendamento = self::findFirst(
                [
                    "conditions" => "id_sala = :id_sala: AND (:dataInicio: BETWEEN data_inicio AND data_fim OR :dataFim: BETWEEN data_inicio AND data_fim )",
                    'bind' => [
                        "id_sala" => $this->id_sala,
                        "dataInicio" => $this->data_inicio,
                        "dataFim" => $this->data_fim
                    ]
                ]
            );
        }

        if ($agendamento) throw new UserException('Sala já agendada por outro usuário neste período');
    }
}