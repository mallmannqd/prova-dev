$('#data').datepicker();
$('#data').datepicker("option", "dateFormat", "dd-mm-yy" );
$('#hora').mask(
    'Hh:Ii',
    {
        translation: {
            'H' : {
                pattern: /[0-2]/
            },
            'h' : {
                pattern: /[0-9]/
            },
            'I' : {
                pattern: /[0-5]/
            },
            'i': {
                pattern: /[0-9]/
            }
        }
    }
);