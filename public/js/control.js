var events = {
    url: '/agendamentos/buscaagendamentos/',
    type: 'POST',
    data: {
        'sala' : $('#sala').val()
    },
    success: function (data) {
        console.log(data);
    }
}

$(document).ready(function() {
    $('#calendar').fullCalendar({
        lang: 'pt-br',
        editable: false,
        defaultDate: moment().format(),
        eventOverlap: true,
        height: 768,
        defaultView: 'agendaDay',
        header: {
            left: 'prev,next, today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        buttonText: {
            today: "Hoje",
            month: "Mês",
            week: "Semana",
            day: "Dia"
        },
        events: events,
        eventDrop: function(event, delta, revertFunc) {

            let start = event.start.format('YYYY-MM-DD HH:mm');
            let end = event.end.format('YYYY-MM-DD HH:mm');

            console.log(start);
            console.log(end);
            console.log(event.id);

            alert(event.title + " Será reagendada para: " + event.start.format('DD-MM-YYYY HH:mm'));

            $.post(
                "/agendamentos/altera/",
                {
                    start: start,
                    end: end,
                    id: event.id
                }
            ).done(
                function (data) {
                    console.log(data);
                    let resposta = JSON.parse(data);
                    if (resposta.success){
                        alert(resposta.message);
                        return;
                    }
                    alert(resposta.message);
                    revertFunc();
                }
            )

        },
        eventClick: function(event) {
            if (event.url) {
                if (!confirm("Cancelar Agendamento da " + event.title + " às " + event.start.format('YYYY-MM-DD HH:mm') + "?")) {
                    return false;
                }
            }
        },
        eventAfterAllRender: function(view){
            $('#loading').hide();
        }

    });

    $('#sala').change(function() {
        $('#calendar').fullCalendar('removeEventSources');
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('addEventSource', {
            url: '/agendamentos/buscaagendamentos/',
            type: 'POST',
            data: {
                'sala' : $('#sala').val()
            },
            success: function (data) {
                console.log(data);
            }
        })
    });
});