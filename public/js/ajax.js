$(document).on('submit', '.ajaxForm', function (e) {
    var me = $(this);
    e.preventDefault();

    if (me.data('requestRunning')) {
        return;
    }

    if ($(this).attr('action') !== undefined) {
        var action = $(this).attr('action');
    } else {
        console.warn('sem action');
        return false;
    }

    if ($(this).attr('method') !== undefined) {
        var method = $(this).attr('method').toUpperCase();
    } else {
        var method = 'POST';
    }

    me.data('requestRunning', true);
    $('input[type="submit"]').attr('disabled', true);

    $.ajax({
        type: method,
        url: action,
        data: $(this).serialize(),
        success: function (ret) {
            if(ret.location){
                window.location(ret.location);
                return true;
            }
            location.reload();
        },
        error: function (obj, status, error) {
            $('#flash-content').html('<div class="alert alert-danger">Erro ao processar dados</div>');
        },
        complete: function () {
            me = null;

            //brz.removeLoading();
            setTimeout(function () {
                $('input[type="submit"]').removeAttr('disabled');
            }, 1500);
        }
    });
});