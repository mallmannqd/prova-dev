$(document).ready(function(){
    utilizadosDominios = $('.progress-bar');

    utilizadosDominios.each(function(dominio){
        var utilizado = $(this).attr('aria-valuenow');
        console.log(utilizado);
        if(utilizado > 0 && utilizado <= 40){
            $(this).addClass('progress-bar-success');
        }else if( utilizado > 40 && utilizado <= 50){
            $(this).addClass('progress-bar-info');
        }else if(utilizado > 50 && utilizado < 60){
            $(this).addClass('progress-bar-warning');
        }else{
            $(this).addClass('progress-bar-danger');
        }
    });
});